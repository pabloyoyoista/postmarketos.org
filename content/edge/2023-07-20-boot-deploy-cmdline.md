title: "/etc/boot/cmdline.txt is no longer used to configure kernel parameters on boot"
date: 2023-07-20
---

A recent update to boot-deploy 0.9.x has dropped support for using
`/etc/boot/cmdline.txt` to configure the kernel parameters used on boot. If you
are currently using this file to configure kernel parameters, please move these
parameters to a new variable named `deviceinfo_kernel_cmdline_append` in the
`/etc/deviceinfo` file.

For example, if you have the following in `/etc/boot/cmdline.txt`:

```
quiet ipv6.disable=1 iomem=relaxed
```

Then you will want to add the following to `/etc/deviceinfo`:

```
deviceinfo_kernel_cmdline_append="quiet ipv6.disable=1 iomem=relaxed"
```

After this manual migration is performed, it is safe to remove
`/etc/boot/cmdline.txt`.

Also see:

- [boot-deploy!29](https://gitlab.com/postmarketOS/boot-deploy/-/merge_requests/29)
