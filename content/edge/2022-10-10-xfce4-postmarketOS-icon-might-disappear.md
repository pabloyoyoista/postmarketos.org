title: "Xfce4: postmarketOS icon might disappear"
date: 2022-10-10
---

In January 2022, the location of the postmarketOS icon was moved from `/usr/share/icons/postmarketOS/logo.svg` to `/usr/share/pixmaps/postmarketos-logo.svg` ([pma commit 5b424a36](https://gitlab.com/postmarketOS/pmaports/-/commit/5b424a36d704d85e7e1ed60042bc0f06c76515a7)). In Xfce4 this caused the disappearance of the icon from the menu button in the top left corner. As a quick fix, a symlink to the old icon location was established ([pma!2916](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2916)). As a clean fix, the new icon location was implemented in the Xfce4 settings ([xfce4-phone!9](https://gitlab.com/postmarketOS/xfce4-phone/-/merge_requests/9) and [pma!2935](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2935)).

Now, 7 month later, the symlink will be removed as a clean-up action ([pma!3503](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3503)). However, the config file with the icon location gets copied into the home config folder `~/.config/xfce4`, therefore it doesn't get updated. On some existing installations removing the symlink causes the disappearence of the icon again. On those installations this needs to be fixed manually by Settings -> Panel -> Items -> Whisker Menu -> Edit (icon) -> Appearance tab -> Icon -> navigate to `/usr/share/pixmaps` (double-click folders) and choose `postmarketos-logo.svg`. Affected installations are:
* Edge installations prior to 10th Feb 2022, icon is expected to get lost after upgrading `postmarketos-ui-xfce4` package.
* Stable installations created as v21.12 or older, icon is expected to get lost when upgrading to future v22.12.

Related issue: [pma#1418](https://gitlab.com/postmarketOS/pmaports/-/issues/1418)
