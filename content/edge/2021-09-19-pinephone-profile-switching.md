title: "PinePhone + Phosh: automatic audio profile switching for calls is broken"
date: 2021-09-19
---

In Phosh on the PinePhone, the sound profile is supposed to switch to "Make a
phone call" when accepting a call. This is currently broken and being looked
into. Until it is fixed, there is no audio during calls.

As workaround, if getting a call, hang up and switch the audio profile manually
in Settings / Audio and then return the call.

Find details in [pma#1237](https://gitlab.com/postmarketOS/pmaports/-/issues/1237).
