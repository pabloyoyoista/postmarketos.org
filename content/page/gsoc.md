title: "postmarketOS ideas for Google Summer of Code 2024"
---
This is the list of project ideas that the postmarketOS
[Core Team](https://wiki.postmarketos.org/wiki/Team_members#Core_Team) is
interested in mentoring. GSoC interns can also propose their very own project
ideas, but we expect them to engage with the community strongly before or during
the application period to get feedback and guidance to improve the proposal. If
you are interested in proposing a project idea, make sure to have a
postmarketOS-capable [device](https://wiki.postmarketos.org/wiki/Devices) and
contact us on [Matrix](https://wiki.postmarketos.org/wiki/Matrix_and_IRC).

## Proposals

### Upstreaming and improving [mobile-config-firefox](https://gitlab.com/postmarketOS/mobile-config-firefox/)

For many years, postmarketOS has been maintaining a small set of CSS and JS
hacks to make desktop firefox more useful on mobile. The project is widely used
outside postmarketOS. We would like to
[upstream](https://gitlab.com/postmarketOS/mobile-config-firefox/-/issues/28)
to Firefox as much as possible, and we have gotten possitive feedback from the
Firefox Linux community to do so. Most of the changes will likely require small
tweaks, and mostly to react to the feedback upstream. We will also provide
help with the management and communication with upstream developers.

#### Requirements

CSS and JS experience. Nice to have would be experience debugging firefox and
creating adaptive designs with CSS and JS.

#### Communication

Matrix (video call and email are secondary comms)

#### Mentors: [Pablo Correa Gomez](https://gitlab.com/pabloyoyoista), [Oliver Smith](https://gitlab.com/ollieparanoid)

#### Project length: Flexible

### pbsplash improvements: IPC, rotation, animations

pbsplash is a tiny utility used at postmarketOS for displaying an annimation
during boot. It needed to be developed to be able to fit into tiny android
partitions, where existing programs would not match the requirements. As any
new tool, there are some clear features still missing, that would improve the
user experience considerably.

#### Requirements

Good C language knowledge. Knowledge on linux graphics would be helpful.

#### Communication

Matrix (video call and email are secondary comms)

#### Mentors: [Clayton Craft](https://gitlab.com/craftyguy), [Caleb Connolly](https://gitlab.com/calebccff)

#### Project length: Medium or long
