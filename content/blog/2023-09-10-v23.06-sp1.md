title: "v23.06 SP1: Through Staging"
title-short: "v23.06 SP1"
date: 2023-09-10
preview: "2023-09/n900_sp1.jpg"
---

[![Nokia N900 with v23.06 SP1 installed, showing a terminal with neofetch, on top of the latest c't magazine with an article about postmarketOS in which the string "Nokia N900" was highlighted manually with a text marker](/static/img/2023-09/n900_sp1.jpg){: class="wfull border" }](/static/img/2023-09/n900_sp1.jpg)

Yes, it is still possible to receive up-to-date software for the Nokia N900 in
2023. You can install postmarketOS v23.06 SP1 on it and upcycle it, possibly as
SSH client on-the-go with its glorious hardware keyboard. Or use it as small
webserver for a home project with built-in uninterruptible power supply, as
mentioned in the c't magazine article in the picture. This service pack brings
quite a few improvements for the N900, such as a power button action prompt
that shows up when you press the power button and an improved terminal with a
scrollbar.

"Through Staging" is the title of the service pack, because for the first time,
it was available in a
[staging repository](https://wiki.postmarketos.org/wiki/Staging_repositories)
before the release. This allowed testing it without building packages locally
beforehand. If you would like to get early access to future service packs,
consider joining the
[testing team](https://wiki.postmarketos.org/wiki/Testing_Team).

## Contents

All devices:

* Phosh upgrade from 0.27.0 to [0.30.0](https://phosh.mobi/releases/rel-0.30.0/)
  (phosh, phoc, squeekboard, phosh-mobile-settings)
* temp/gtk+3.0: fix Phosh boot splash ({{MR|4332|pmaports}})
* temp/gtk+3.0: upgrade to 3.24.37-2pureos3 ({{MR|4261|pmaports}})
* main/postmarketos-base-ui: depend on tzdata ({{MR|4175|pmaports}})
* main/postmarketos-base: migrate wrong timezone configurations ({{MR|4308|pmaports}}) (*)
* cross/crossdirect: improve rust handling ({{MR|4234|pmaports}})
* linux-postmarketos-omap: upgrade to 6.4.3 ({{MR|4253|pmaports}}) (*)
* linux-postmarketos-exynos4: upgrade to 6.4.2 ({{MR|4241|pmaports}}) (*)
* linux-postmarketos-qcom-sm6350: upgrade to 6.4.2 ({{MR|4236|pmaports}}) (*)
* main/mobile-config-firefox: upgrade to 4.0.3 ({{MR|4232|pmaports}}) (*)
* temp/gnome-shell-mobile: add desktop-file-utils dependency ({{MR|4210|pmaports}}) (*)
* main/postmarketos-mkinitfs-hook-\*: show Loading splash once done ({{MR|4212|pmaports}}) (*)

Nokia N900:

* Restore modem ({{MR|4320|pmaports}}, {{MR|4282|pmaports}})
* Let user choose power button action ({{MR|4312|pmaports}})
* Switch to urxvt terminal ({{MR|4312|pmaports}})
* Increase terminal and i3 font sizes ({{MR|4363|pmaports}})
* Fix hwkbd on unl0kr ({{MR|4320|pmaports}})
* Fix volume keys on N900 ({{MR|4228|pmaports}})
* Use elogind for suspend ({{MR|4312|pmaports}})
* Drop broken conky default config ({{MR|4309|pmaports}})
* Do not open terminal for nagbar actions ({{MR|4320|pmaports}})
* Re-add twl for basic soc reset ({{MR|4321|pmaports}}) (*)
* Disable twl off idle features ({{MR|4317|pmaports}}) (*)
* Block non-critical modules ({{MR|4227|pmaports}}) (*)

PINE64 PineBook Pro:

* Enable s2idle in elogind ({{MR|4194|pmaports}})

Purism Librem 5:

* Kernel upgrade to 6.4.5pureos1 ({{MR|4355|pmaports}}) (*)

`(*)`: These fixes were already backported to v23.06 before releasing SP1.

## Notes

For users upgrading from before v23.06 SP1: some of the N900 improvements
require manually copying the defaults from `/etc/skel` to your home dir.

Running out of space after installing v23.06 on Android devices is a known bug
({{issue|2235|pmaports}}). Read the issue for workarounds, a fix is being
developed.

## How to get it

Find the most recent images at our [download page](/download/). Existing users
of the v23.06 release will receive this service pack automatically on their
next system update. If you read this blog post right after it was published, it
may take a bit until binary packages and new images are available.

Thanks to everybody who made this possible, especially our amazing community
members and upstream projects.
