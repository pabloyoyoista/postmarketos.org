title: "postmarketOS Release: v21.06 Service Pack 2"
date: 2021-09-13
preview: "2021-09/vlinder.jpg"
---

[![](/static/img/2021-09/vlinder_thumb.jpg){: class="wfull border"}](/static/img/2021-09/vlinder.jpg)

The second service pack for postmarketOS v21.06 has been released. As usually,
it brings improvements from edge to the stable release of postmarketOS, after
careful testing by developers and brave community members on living on the
edge.

The v21.06.2 service pack contains:

* [Megapixels 1.3.0](https://git.sr.ht/~martijnbraam/megapixels/refs/1.3.0),
  with performance improvements and new image post-processing. As you've
  guessed, the image above was shot with this Megapixels release on a
  PinePhone. If you have a similar setup of butterflies and flowers in your
  garden, or something else that is nice to look at, give it a try!
* [Phosh 0.12.1](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.12.1)
* [megi's 5.13-20210819-1611 kernel](https://github.com/megous/linux/releases/tag/orange-pi-5.13-20210819-1611)
  for the PinePhone and PineTab
* [Light daemon](http://haikarainen.github.io/light/) is now available for
  usage in Sxmo
* gpodder-adaptive 3.10.21, with major
  [performance and UI fixes](https://fosstodon.org/@ollieparanoid/106631629628073756)
* [u-boot-pinephone 2021.07](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/2315)
  with a new crust release that improves battery life significantly in standby

Find the most recent images at our [download page](/download/). Existing users
of the v21.06 release will receive this service pack automatically on their
next system update.

Thanks to everybody who made this possible, especially our amazing community
members and upstream projects.

## Comments

* [Mastodon](https://fosstodon.org/@postmarketOS/106921145673356508)
* [Lemmy](https://lemmy.ml/post/80650)
<small>
* [Reddit](https://old.reddit.com/r/postmarketOS/comments/pn307t/postmarketos_release_v2106_service_pack_2/)
* [HN](https://news.ycombinator.com/item?id=28505786)
</small>
